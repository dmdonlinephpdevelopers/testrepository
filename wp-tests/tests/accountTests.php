<?php
namespace cUTest;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class accountTests_testClass extends wpTestAssets {

 // public $runTestFunctions = false;
  
  /* @var $dmdApiConnectObject fapi */
  public $dmdApiConnectObject;  
  public $rulesArray = [];
  private $emailCount = 0;

  function __construct() {
    $this->resetMockData();
    createRegisterStepTests($this);
  }

  public function resetMockData($deepReset = false) {
    if ($deepReset) {
      $this->mockData = [];
    } else {
      $this->mockData = [
          'Fname' => 'test',
          'Lname' => 'test',
          'Phone-code' => '',
          'Phone' => (string) rand(1111111111, 9999999999),
          'countryID' => '26',
          'accountTypeID' => '1',
          'questionnairey_step' => '1',
          'accept_terms' => 'true',
          'IsTest' => true
      ];
    }
  }

  private function generateMail() {
    $this->mockData['Email'] = "test_unitTest_" . date('Ymd') . '_' . rand() . $this->emailCount . "@" . "yopmail.com";
    $this->emailCount++;
  }

  private function applyLeadIDRule() {
    $this->addRule($this->rulesArray, 'LeadID', function($data) {
      return strlen((string) $data) > 6;
    }, null, true);
  }

  public function beforeEach() {
    $this->dmdApiConnectObject = new \fapi($this->mockData);
    
    $this->rulesArray = [];
    $this->addRule($this->rulesArray, "IsSuccessful", 1, "==", true);
   
  }

  public function afterEach($testManager, $resultData) {
    $testManager->setValidationRules($this->rulesArray);
    OutputValidation::validate($this->rulesArray, (array) $resultData);
  }

  //*****TESTS*******//  
  public function CreateLead_testStep_init() {
    $this->generateMail();
  }

  public function CreateLead_testStep($testManager) {
    $testManager->setSentData($this->dmdApiConnectObject->data);
    $leadResult = $this->dmdApiConnectObject->CreateLead();

    return $leadResult;
  }

  public function CreateLead_testStep_done($testManager, $resultData) {
    $this->mockData['LeadID'] = $resultData->LeadID;
  }

  //** Open Account Test **//
  public function OpenAccount_testStep_init() {
    //Make sure that we have data for login
    $this->dependencyCheck(get_class(), "CreateLead_testStep");
    $this->mockData['currencyID'] = 1;
  }

  public function OpenAccount_testStep($testManager) {
    $testManager->setSentData($this->dmdApiConnectObject->data);
    return $this->dmdApiConnectObject->OpenAccount();
  }

  public function OpenAccount_testStep_done($testManager, $resultData) {
    $this->applyLeadIDRule();
  }

  //*********//
  //** Login Test **//  
  public function Login_testStep_init() {

    $this->resetMockData();
    //Make sure that we have data for login
    $this->dependencyCheck(get_class(), "OpenAccount_testStep");

    $openAccountResultData = $this->getTestResultData(get_class(), "OpenAccount_testStep");

    //Add Data for the login function
    $this->mockData['Email'] = $openAccountResultData->Email;
    $this->mockData['Password'] = $openAccountResultData->Password;
  }

  public function Login_testStep($testManager) {
    $testManager->setSentData($this->dmdApiConnectObject->data);
    return $this->dmdApiConnectObject->Login();
  }

  public function Login_testStep_done() {
    $this->addRulesWrapper($this->rulesArray, "LoginResult");
    $this->addRule($this->rulesArray, 'Password', null, '!=', true);
    $this->addRule($this->rulesArray, 'Culture', null, '!=', true);
    $this->applyLeadIDRule();
  }

  //*********//
  //** Change Password Test **//
  public function ChangePassword_testStep_init() {

    $this->resetMockData();
    $this->dependencyCheck(get_class(), "Login_testStep");

    $loginResultData = $this->getTestResultData(get_class(), "Login_testStep");

    $this->mockData['Email'] = $loginResultData->LoginResult->Email;
    $this->mockData['OldPassword'] = $loginResultData->LoginResult->Password;
    $this->mockData['Culture'] = $loginResultData->LoginResult->Culture;

    $this->tempData['NewPassword'] = "testUnitPasswordChangeTest1234#";
    $this->mockData['Password'] = $this->tempData['NewPassword'];
  }

  public function ChangePassword_testStep($testManager) {
    $testManager->setSentData($this->dmdApiConnectObject->data);
    return $this->dmdApiConnectObject->ChangePassword();
  }

  public function ChangePassword_testStep_done() {
    $this->addRulesWrapper($this->rulesArray, 'ChangePasswordResult');
  }

  //**********//
  //** Reset Password Test **//
  public function ResetPassword_testStep_init() {
    $this->dependencyCheck(get_class(), "Login_testStep");

    $this->resetMockData();
    $this->mockData['Email'] = $this->getTestResultData(get_class(), "Login_testStep")->LoginResult->Email;
  }

  public function ResetPassword_testStep($testManager) {
    $testManager->setSentData($this->dmdApiConnectObject->data);
    return $this->dmdApiConnectObject->ResetPassword();
  }

  public function ResetPassword_testStep_done() {
    $this->addRulesWrapper($this->rulesArray, 'ResetPasswordResult');
  }

  //**********//
  //** Get account balance Test **//
  public function getAccountBalance_testStep_init() {
    $this->dependencyCheck(get_class(), "Login_testStep");

    $loginResultData = $this->getTestResultData(get_class(), "Login_testStep");
    $this->mockData['Email'] = $loginResultData->LoginResult->Email;
    $this->mockData['Username'] = $loginResultData->LoginResult->Username;
    $this->mockData['Culture'] = $loginResultData->LoginResult->Culture;    
  }

  public function getAccountBalance_testStep($testManager) {
    $testManager->setSentData($this->dmdApiConnectObject->data);
    return $this->dmdApiConnectObject->getAccountBalance();
  }

  //** Get account balance by Date Test **//
  public function getAccountBalancebyDate_testStep_init($testManager) {
    
    $this->dependencyCheck(get_class(), "getAccountBalance_testStep");

    $now = time(); // or your date as well
    $transactionDate = strtotime("2016-09-27");
    $datediff = $now - $transactionDate;

    $this->mockData['Period'] = floor($datediff / (60 * 60 * 24));    
    $this->mockData['Username'] = "17527"; //We know this user have transaction data
  }

  public function getAccountBalancebyDate_testStep($testManager) {
    $testManager->setSentData($this->dmdApiConnectObject->data);
    return $this->dmdApiConnectObject->getAccountBalancebyDate();
  }
  
  public function getAccountBalancebyDate_testStep_done(){
    $this->removeRule($this->rulesArray, 'IsSuccessful');
     
    $this->addRule($this->rulesArray, 0, function($data){
      
      return isset($data->ActionType);
    }, null, true);
  }

}
