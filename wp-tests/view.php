<?php

require_once './vendor/autoload.php';

$unitTest = new cUTest\unitTest(__DIR__); //Configure the root directory

if (isset($_GET['test'])) {
  
  header('Content-Type: application/json');
  echo $unitTest->runTest("tests"); //test is the sub directory where the tests are
  
} else {
  
  $unitTest->runView("?test=true"); //?test=true is the ajax url will be passed to the HTML
  
}
